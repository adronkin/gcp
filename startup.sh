#!/bin/bash

# instal and start mongo
apt --assume-yes update
apt --assume-yes install gnupg wget
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
apt --assume-yes update
apt install --assume-yes mongodb-org
systemctl enable --now mongod

# install ruby
apt update
apt install -y ruby-full ruby-bundler build-essential

# deploy project
apt --assume-yes update
apt install --assume-yes git
git clone -b monolith https://github.com/express42/reddit.git /opt/reddit/
cd /opt/reddit/ && bundle install
cd /opt/reddit/ && puma -d
