gcloud compute instances create reddit-app\
  --project=infra-324415 \
  --boot-disk-size=10GB \
  --image-family=debian-10 \
  --image-project=debian-cloud \
  --machine-type=e2-small \
  --tags=puma-server \
  --metadata-from-file=startup-script=/Users/artemdronkin/project/gcp/startup.sh \
  --restart-on-failure


gcloud compute instances add-metadata reddit-app \
  --project infra-324415 \
  --metadata-from-file=startup-script=/Users/artemdronkin/project/gcp/install_mongodb.sh


gcloud compute firewall-rules create test-rule \
  --project=infra-324415 \
  --allow=tcp:9090 \
  --network=default \
  --priority=1000 \
  --direction=INGRESS \
  --target-tags=puma-server \
  --source-ranges="0.0.0.0/0" \
  --description="Test"
