#!/bin/bash

echo "deb https://repo.pritunl.com/stable/apt buster main" | sudo tee /etc/apt/sources.list.d/pritunl.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
apt --assume-yes update
apt --assume-yes install pritunl
apt --assume-yes install gnupg
apt --assume-yes install wget
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
apt install --assume-yes mongodb-org
systemctl enable --now pritunl
systemctl enable --now mongod
